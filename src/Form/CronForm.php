<?php

namespace Drupal\css_js_performance_improvement\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Class CronForm.
 */
class CronForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'css_js_performance_improvement.cron',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cron_form';
  }

  private function getAllRoles() {
    return array_combine(
      array_keys(Role::loadMultiple()), 
      array_keys(Role::loadMultiple()),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('css_js_performance_improvement.cron');

    /**
     * Settings fieldset.
     */
    $form['settings'] = [
      '#type' => 'fieldset',
      '#title' => t('Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['settings']['cron_days'] = [
      '#type' => 'number',
      '#title' => $this->t('Days for the CRON:'),
      '#description' => $this->t('Insert the number of the days for the CRON.<br>The default CRON behaviour is used when you insert 0.'),
      '#default_value' => $config->get('cron_days'),
    ];
    $form['settings']['excluded_roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('Exclude some roles:'),
      '#options' => $this->getAllRoles(),
      '#description' => $this->t('Specify the roles that should not be interested by aggregation process.'),
      '#default_value' => $config->get('excluded_roles'),
    ];

    /**
     * CSS Aggregation fieldset.
     */
    $form['css_aggregation'] = [
      '#type' => 'fieldset',
      '#title' => t('CSS Aggregation'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['css_aggregation']['css_escluded_path'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Insert the paths for the excluded aggregation (only one path at line, if you want to exclude the sub-paths of a specific path insert /*):'),
      '#description' => $this->t('Warning: it can have unwanted effects.'),
      '#default_value' => $config->get('css_escluded_path'),
    ];
    $form['css_aggregation']['css_equal_path'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Specify the sub-paths with the same CSS file (you can use the notation sub-path::filename each line):'),
      '#description' => $this->t('Warning: it can have unwanted effects.'),
      '#default_value' => $config->get('css_equal_path'),
    ];
    $form['css_aggregation']['module_activated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate the CSS aggregation process.'),
      '#default_value' => $config->get('module_activated'),
    ];

    /**
     * JS Aggregation fieldset.
     */
    $form['js_aggregation'] = [
      '#type' => 'fieldset',
      '#title' => t('JS Aggregation'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['js_aggregation']['js_escluded_path'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Insert the paths for the excluded aggregation (only one path at line, if you want to exclude the sub-paths of a specific path insert /*):'),
      '#description' => $this->t('Warning: it can have unwanted effects.'),
      '#default_value' => $config->get('js_escluded_path'),
    ];
    $form['js_aggregation']['js_escluded_files'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Specify the excluded JS files (each line):'),
      '#description' => $this->t('Warning: it can have unwanted effects.<br>You can specify also a content term as "core" for "/core/modules/..".'),
      '#default_value' => $config->get('js_escluded_files'),
    ];

    $form['js_aggregation']['js_module_activated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate the JS aggregation process.'),
      '#default_value' => $config->get('js_module_activated'),
    ];

    /**
     * Debug fieldset.
     */
    $form['debug'] = [
      '#type' => 'fieldset',
      '#title' => t('Debug'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['debug']['debug_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate the debug mode.'),
      '#description' => $this->t('Enabling this you will see some log in watchdog.'),
      '#default_value' => $config->get('debug_enabled'),
    ];

    /**
     * Information fieldset.
     */
    $form['information'] = [
      '#type' => 'fieldset',
      '#title' => t('General information'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['information']['info'] = [
      '#type' => 'item', 
      '#description' => $this->t('Date of the last cron running (configured): ') . '<b>' . ($config->get('current_day') ?: 'not set yet') . '</b>',
    ];
    $form['information']['warning'] = [
      '#type' => 'item',
      '#description' => $this->t('
        <div role="contentinfo" aria-label="Warning message" class="messages messages--warning">
          <h2 class="visually-hidden">Warning message</h2>
          At the same time as saving the configuration, also Drupal cache will be cleared.<br><b>If you know what you are doing, go on without fear!</b>
        </div>'),
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('css_js_performance_improvement.cron')
      ->set('cron_days', $form_state->getValue('cron_days'))
      ->set('module_activated', $form_state->getValue('module_activated'))
      ->set('css_escluded_path', $form_state->getValue('css_escluded_path'))
      ->set('css_equal_path', $form_state->getValue('css_equal_path'))
      ->set('excluded_roles', $form_state->getValue('excluded_roles'))
      ->set('js_escluded_path', $form_state->getValue('js_escluded_path'))
      ->set('js_escluded_files', $form_state->getValue('js_escluded_files'))
      ->set('js_module_activated', $form_state->getValue('js_module_activated'))
      ->set('debug_enabled', $form_state->getValue('debug_enabled'))
      ->save();
    
    drupal_flush_all_caches();
  }
}
