<?php

namespace Drupal\css_js_performance_improvement\Service;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use MatthiasMullie\Minify;

/**
 * Class DefaultService.
 */
final class DefaultService {

  /**
   * @var FileSystemInterface $fileSystem
   */
  protected $fileSystem;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * @var $CSSMinifier
   */
  protected $CSSMinifier;

  /**
   * @var $JSMinifier
   */
  protected $JSMinifier;


  /**
   * {@inheritdoc}
   */
  public function __construct(FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger) {
    $this->fileSystem = $file_system;
    $this->logger = $logger->get('css_js_performance_improvement');

    // Thanks to: https://github.com/matthiasmullie/minify
    // we can minify CSS and JS without any other effort.
    $this->CSSMinifier = new Minify\CSS();
    $this->JSMinifier = new Minify\JS();
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('logger'),
    );
  }


  public function generate_aggregated_css($css_files, $file_name, $debug) {
    $file_path = 'public://css_js_files/css_js_performance_improvement/css/';
    $logger_paths = [];

    foreach($css_files as $name => $data) {
      $data_real_path = $this->fileSystem->realpath($css_files[$name]['data']);
      $this->CSSMinifier->add(file_get_contents($data_real_path));

      if($debug) $logger_paths[] = 'Filename: ' . $name . ' :: Filepath: ' . $data_real_path;
    } 
    
    $this->CSSMinifier->minify($file_path . $file_name);
    if(!empty($logger_paths)) $this->logger->notice('[CSS]: <pre>' . print_r($logger_paths, true) . '</pre>');
  }


  public function generate_aggregated_js($js_files, $file_name, $debug) {
    $file_path = 'public://css_js_files/css_js_performance_improvement/js/';
    $logger_paths = [];

    foreach($js_files as $name => $data) {
      $data_real_path = $this->fileSystem->realpath($js_files[$name]['data']);
      $this->JSMinifier->add(file_get_contents($data_real_path));

      if($debug) $logger_paths[] = 'Filename: ' . $name . ' :: Filepath: ' . $data_real_path;
    }

    $this->JSMinifier->minify($file_path . $file_name);
    if(!empty($logger_paths)) $this->logger->notice('[JS]: <pre>' . print_r($logger_paths, true) . '</pre>');
  }


  public function remove_all_folder_files($file_path) {
    $files = glob($file_path);
    $operation_status = true;

    foreach($files as $file) { 
      if(!unlink($file)) {
        $operation_status = false;
      }
    } return $operation_status; 
  }
}
